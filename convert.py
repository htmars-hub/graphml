import os
import json
import subprocess
import argparse


def parse_args():
    parser = argparse.ArgumentParser('Run the model conversion')

    parser.add_argument('-i', '--input', help='Path to the exported model')
    parser.add_argument('-o', '--output', help='Path to save the converted model')

    return parser.parse_args()


args = parse_args()

exported_dir = args.input
convert_out = args.output

subprocess.call(['tensorflowjs_converter',
                 '--input_format=tf_saved_model',
                 '--output_format=tfjs_graph_model',
                 exported_dir, convert_out])

model_file = os.path.join(convert_out, 'model.json')

model = json.loads(open(model_file).read())
model_data = model

if 'features' in model_data['signature']['inputs']:
    model_data['signature']['inputs'] = {
        'adjacency': model_data['signature']['inputs']['adjacency'],
        'features': model_data['signature']['inputs']['features']
    }
elif 'features:0' in model_data['signature']['inputs']:
    model_data['signature']['inputs'] = {
        'adjacency:0': model_data['signature']['inputs']['adjacency:0'],
        'features:0': model_data['signature']['inputs']['features:0']
    }

model_data["weightsManifest"][0]["paths"] = ["group1-shard1of1.dat"]

# Add meta data (model version and hyper-parameters)
meta_data_file_path = os.path.sep.join([exported_dir, 'meta_data.json'])
with open(meta_data_file_path, 'r', encoding='utf-8') as f:
    meta_data = json.load(f)
model_data.update(meta_data)

json.dump(model_data, open(model_file, 'wt'))

subprocess.call(['cp',
                 os.path.join(convert_out, 'group1-shard1of1.bin'),
                 os.path.join(convert_out, 'group1-shard1of1.dat')])
