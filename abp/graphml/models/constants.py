# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import tensorflow.keras as keras

from . import callbacks
from .metrics import precision, recall, f1_score


HYPERPARAMS_TYPES = {
    'learning_rate': float,
    'loss': str,
    'validation_split': float,
    'epochs': int,
    'batch_size': int,
    'armaconv_dropout': float,
    'l2_reg': float,
    'optimizer_decay': float,
    'nodes': int,
    'features': int,
    'features_dropout': float,
}


class ModelConstants:
    MODEL_VERSION = '0.0.1'
    LEARNING_RATE = 1e-3
    LOSS = 'binary_crossentropy'
    METRICS = ['acc', precision, recall, f1_score]
    ES_PATIENCE = 10  # Patience for early stopping
    TRAINING_CALLBACKS = [
        keras.callbacks.TensorBoard(
            log_dir='.logs/', histogram_freq=0, write_graph=True,
            write_images=True,
        ),
        keras.callbacks.EarlyStopping(monitor='loss', patience=10),
        callbacks.FloydHubTrainingMetricsCallback(),
    ]
    VALIDATION_SPLIT = .2
    EPOCHS = 1
    BATCH_SIZE = None


class GraphConvNet(ModelConstants):
    ARMACONV_DROPOUT = .5
    L2_REG = 5e-5
    OPTIMIZER_DECAY = 1e-5
    EPOCHS = 2000
    BATCH_SIZE = 128
    FEATURES_DROPOUT = .2
