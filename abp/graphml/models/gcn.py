# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import tensorflow.keras as keras
import spektral

from . import constants as const


def get_graph_conv_net(hyperparams, use_depth=False):
    """Get a configured convolutional neural network.

    Parameters
    ----------
    hyperparams: abp.graphml.models.utils.HyperParameters
        Class containing the hyperparameters used to configure the model.
    use_depth: bool
        Wether to use a depth parameter.

    Returns
    -------
    keras.Model
        With the pre-defined architecture.

    """
    nodes = hyperparams.get('nodes')
    features = hyperparams.get('features')

    # Defining the model
    x_in = keras.layers.Input(shape=(nodes, features), name='features')
    filter_in = keras.layers.Input(shape=(nodes, nodes), name='adjacency')
    if use_depth:
        depth_in = keras.layers.Input(shape=(1,), name='depth')

    armaconv_dropout = hyperparams.get(
        'armaconv_dropout', const.GraphConvNet.ARMACONV_DROPOUT,
    )
    l2_reg = hyperparams.get('l2_reg', const.GraphConvNet.L2_REG)

    gc_layer1 = spektral.layers.ARMAConv(
        64, 2, activation='relu',
        dropout_rate=armaconv_dropout,
        kernel_regularizer=keras.regularizers.l2(l2_reg),
    )([x_in, filter_in])

    gc_layer2 = spektral.layers.ARMAConv(
        64, 2, activation='relu',
        dropout_rate=armaconv_dropout,
        kernel_regularizer=keras.regularizers.l2(l2_reg),
    )([gc_layer1, filter_in])

    attn_pool = spektral.layers.GlobalAttentionPool(64)(gc_layer2)

    processed_features = None

    if use_depth:
        features_dropout = hyperparams.get(
            'features_dropout', const.GraphConvNet.FEATURES_DROPOUT,
        )
        concatenated_features = keras.layers.Concatenate()(
            [attn_pool, depth_in]
        )
        processed_features = keras.layers.Dropout(
            features_dropout,
        )(concatenated_features)
    else:
        processed_features = attn_pool

    dense_layer1 = keras.layers.Dense(128, activation='relu')(
        processed_features
    )

    output_layer = keras.layers.Dense(2, activation='sigmoid',
                                      name='prediction')(dense_layer1)

    learning_rate = hyperparams.get(
        'learning_rate', const.GraphConvNet.LEARNING_RATE
    )
    decay = hyperparams.get(
        'optimizer_decay', const.GraphConvNet.OPTIMIZER_DECAY,
    )
    loss = hyperparams.get('loss', const.GraphConvNet.LOSS)

    # Building the model
    inputs = [filter_in, x_in]
    if use_depth:
        inputs = [filter_in, x_in, depth_in]
    model = keras.Model(inputs=inputs, outputs=output_layer)
    model.compile(
        optimizer=keras.optimizers.Adam(
            lr=learning_rate,
            decay=decay,
        ),
        loss=loss,
        metrics=const.GraphConvNet.METRICS,
    )

    return model
