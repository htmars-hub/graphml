# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

from collections import defaultdict

__all__ = ['PREDICT_HANDLERS']


def predict_keras_model(model, data, as_probs=False, verbose=0):
    """Run a prediction against a keras model.

    Parameters
    ----------
    model: keras.Model
        The pre-trained model to run the prediction against.
    data: tuple (np.ndarray, np.ndarray)
        Where the two arrays signify:
            - X_in: np.ndarray
                The features matrices.
            - adj_mxs_in: np.ndarray
                The adjacency matrices.
    as_probs: bool
        Whether to return the prediction as probabilities or not. Default False
    verbose: int
        The verbosity level that will be passed to the model when running the
        prediction. Default 0.

    Returns
    -------
    numpy.ndarray
        With the predicted labels if `as_probs` is False and the predicted
        probabilities, otherwise.

    """
    if len(data) != 2:
        raise ValueError('The length of the data tuple has to be exactly 2. '
                         'Found {}. Check the docstring for further '
                         'information'.format(len(data)))

    if not as_probs:
        return model.predict(data, verbose=verbose)

    return model.predict_proba(data, verbose=verbose)


def predict_unsupported(*args, **kwargs):
    raise Exception('Predicting for this model is not currently supported.')


PREDICT_HANDLERS = defaultdict(predict_unsupported)

PREDICT_HANDLERS['graph_conv_net'] = predict_keras_model
