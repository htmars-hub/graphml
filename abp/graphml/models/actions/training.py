# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

from collections import defaultdict
import os

from abp.graphml.models import constants as const
from abp.graphml.models import utils

import tensorflow.keras as keras

__all__ = ['TRAIN_HANDLERS']


def train_keras_model(const_cls=const.ModelConstants):
    def train_inner_fn(model, data, hyperparams, save_location=None,
                       export_location=None, save_best_only=False,
                       checkpoints_dir=None, use_depth=False):
        """Train a keras-based model.

        Parameters
        ----------
        model: keras.Model
            The model to be trained.
        data: tuple (np.ndarray, np.ndarray, np.ndarray, np.ndarray?)
            Where the expected elements are (in this order):
                - adj_mxs_in: np.ndarray
                    The adjacency matrices of the training data.
                - X_in: np.ndarray
                    The features matrices of the training data.
                - Y_in: np.ndarray
                    The labels of the training data.
                - depth: np.ndarray [optional]
                    Depth of posts (number in a feed)
        hyperparams: abp.graphml.utils.HyperParameters
            The hyperparameters used in training.
        save_location: str
            Path to the file where to save the model (if any). If `None`, the
            model will not be saved to disk.
        export_location: str
            Path to the directory where to save the model in a tf.js-compatible
            format. If `None`, the model will not be exported. Default `None`.
        save_best_only: boolean
            Whether to save only the best performing model or not.
        use_depth: boolean
            Whether to use the depth parameter in data

        Returns
        -------
        keras.History
            With all the relevant information related to the training process.

        """
        if use_depth:
            if len(data) != 4:
                raise ValueError('The data parameter must have exactly 4 '
                                 'arguments. Got {}. Check the docstring for '
                                 'further information.'.format(len(data)))
            data_in = [data[0], data[1], data[3]]
        else:
            if len(data) != 3:
                raise ValueError('The data parameter must have exactly 3 '
                                 'arguments. Got {}. Check the docstring for '
                                 'further information.'.format(len(data)))
            data_in = [data[0], data[1]]
        labels = data[2]

        checkpoint_path = None
        training_callbacks = const.ModelConstants.TRAINING_CALLBACKS
        if save_location and save_best_only:
            os.makedirs(checkpoints_dir)
            checkpoint_path = os.path.join(
                checkpoints_dir, 'best_checkpoint.hdf5',
            )
            training_callbacks.append(
                keras.callbacks.ModelCheckpoint(
                    checkpoint_path, monitor='val_precision', verbose=1,
                    save_best_only=True, mode='max',
                ),
            )

        batch_size = hyperparams.get('batch_size', const_cls.BATCH_SIZE)
        validation_split = hyperparams.get(
            'validation_split', const_cls.VALIDATION_SPLIT,
        )
        epochs = hyperparams.get('epochs', const_cls.EPOCHS)

        history = model.fit(data_in, labels,
                            batch_size=batch_size,
                            validation_split=validation_split,
                            epochs=epochs,
                            callbacks=training_callbacks)

        model_to_save = model
        if save_best_only:
            model_to_save.load_weights(checkpoint_path)

        if save_location is not None:
            model_to_save.save(save_location)

        if export_location is not None:
            utils.export_keras_model(model_to_save, export_location,
                                     hyperparams, const_cls)

        return history

    return train_inner_fn


def train_unsupported(*args, **kwargs):
    raise Exception('Training for this model is not currently supported.')


TRAIN_HANDLERS = defaultdict(train_unsupported)

TRAIN_HANDLERS['graph_conv_net'] = train_keras_model(const.GraphConvNet)
