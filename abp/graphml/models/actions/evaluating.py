# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

from collections import defaultdict

from abp.graphml.models import constants as const
from abp.graphml.models.actions import training, predicting


def evaluate_keras_model(const_cls=const.ModelConstants):
    def eval_inner_fn(model, test_data, hyperparams, train_data=None,
                      eval_fns=None):
        """Evaluate a keras model.

        Parameters
        ----------
        model: keras.Model
            The model to evaluate.
        test_data: tuple (np.ndarray, np.ndarray, np.ndarray)
            The test data to evaluate the model against. The three elements
            of the tuple are (in this order):
                - X_in: np.ndarray
                    The features matrices.
                - adj_mxs_in: np.ndarray
                    The adjacency matrices.
                - Y_in: np.ndarray
                    The labels.
        hyperparams: abp.graphml.models.utils.HyperParameters
            Class containing the hyperparameters used to configure the model.
        train_data: tuple (np.ndarray, np.ndarray, np.ndarray)
            The train data used if you want to re-train the model. The
            structure if identical to the one of the `test_data` tuple. If
            `None`, the model will not be trained. Default `None`.
        eval_fns: dict (str -> function)
            Any extra evaluation metrics that are not directly supported by
            keras, but we want to run. Each function should have the
            following signature:
                (predicted_labels, true_labels) => Union(iterable, float)
            If `None`, this argument will be ignored. Default `None`.

        Returns
        -------
        dict (str -> Union(iterable, float))
            Mapping metric names to the results.

        """
        if len(test_data) != 3:
            raise ValueError('The data parameter must have exactly 3 '
                             'arguments. Got {}. Check the docstring for '
                             'further information.'.format(len(test_data)))

        if train_data is not None:
            # We re-train the model
            training.train_keras_model(const_cls)(model, train_data,
                                                  hyperparams)

        eval_result = {}
        data_in = [test_data[0], test_data[1]]
        true_labels = test_data[2]

        raw_results = model.evaluate(data_in, true_labels,
                                     batch_size=const_cls.BATCH_SIZE)

        eval_result['loss'] = raw_results[0]
        for name, result in zip(const_cls.METRICS, raw_results[1:]):
            eval_result[name] = result

        if eval_fns is None:
            return eval_result

        predicted_labels = predicting.predict_keras_model(model, data_in)

        for metric, func in eval_fns.items():
            eval_result[metric] = func(predicted_labels, true_labels)

        return eval_result

    return eval_inner_fn


def eval_unsupported(*args, **kwargs):
    raise Exception('Evaluating for this model is not currently supported.')


EVAL_HANDLERS = defaultdict(eval_unsupported)

EVAL_HANDLERS['graph_conv_net'] = evaluate_keras_model(const.GraphConvNet)
