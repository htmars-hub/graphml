# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import os
import json
import logging
import shutil

import tensorflow as tf
import tensorflow.keras as keras

from . import constants as const
from . import exceptions as error


def export_keras_model(model, export_location, hyperparameters, const_cls,
                       forced=False):
    """
    Export a pre-trained keras model in a tf.js-readable format.

    Parameters
    ----------
    model: keras.Model
        The pre-trained model that will be saved.
    export_location: str
        Path where to save the model in.
    forced: bool
        Whether to forcibly overwrite the previously exported model or not.

    """
    try:
        tf.saved_model.save(model, export_location)
    except AssertionError as err:
        if 'Export directory already exists' not in str(err):
            raise
        logging.warning(
            'Export directory {} already exists!'.format(export_location),
        )

        if forced:
            shutil.rmtree(export_location)
            tf.saved_model.save(model, export_location)

        # TODO: figure out a policy of exporting the model, even if forced is
        # TODO: set to False. (e.g.: only save if it outperforms the previous
        # TODO: model).

    meta_data = hyperparameters.__dict__
    meta_data.update({'model_version': const_cls.MODEL_VERSION})
    meta_data_file_path = os.path.sep.join([export_location, 'meta_data.json'])
    with open(meta_data_file_path, 'w', encoding='utf-8') as f:
        json.dump({'graphml': meta_data}, f)


class HyperParameters:

    def add(self, name, value):
        if (name in const.HYPERPARAMS_TYPES and
                isinstance(value, const.HYPERPARAMS_TYPES[name])):
            self.__setattr__(name, value)
            return True

        raise error.HyperparameterTypeError(name, value)

    def get(self, name, default=None):
        if name in self.__dict__:
            return self.__getattribute__(name)
        return default


def load_keras_model(path):
    """Load a saved keras model.

    Parameters
    ----------
    path: str
        The path to the keras model to be loaded.

    Returns
    -------
    keras.Model
        The loaded model.

    """
    return keras.models.load_model(path)
