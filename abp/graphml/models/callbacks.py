# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import json

import tensorflow.keras as keras


class FloydHubTrainingMetricsCallback(keras.callbacks.Callback):

    METRICS_TO_LOG = {'f1_score', 'val_f1_score', 'precision',
                      'val_precision', 'recall', 'val_recall'}

    def on_epoch_end(self, epoch, logs=None):
        if logs is None:
            return

        for metric in self.METRICS_TO_LOG:
            log = {'metric': metric, 'value': logs.get(metric), 'epoch': epoch}
            print(json.dumps(log))
