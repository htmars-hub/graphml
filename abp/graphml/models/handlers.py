# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

from collections import defaultdict

from .gcn import get_graph_conv_net


__all__ = ['CLASSIFIERS']


def unsupported_handler(*args, **kwargs):
    """Get handler for unsupported models."""
    raise Exception('Unsupported model in config!')


def unimplemented_handler(*args, **kwargs):
    """Get default handler to be used for unimplemented."""
    raise NotImplementedError()


def graph_convnet_handler(hyperparams, use_depth=False, *args, **kwargs):
    """Get a configured Graph Convolutional Network.

    Parameters
    ----------
    hyperparams: abp.graphml.models.utils.HyperParameters
        Class containing the hyperparameters used in configuring the model.

    """
    return get_graph_conv_net(hyperparams, use_depth)


CLASSIFIERS = defaultdict(unsupported_handler)

CLASSIFIERS['graph_conv_net'] = graph_convnet_handler
