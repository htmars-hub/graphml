# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import argparse
import logging
import sys

from abp.graphml.pipeline import Pipeline, run_pipeline_step


def run_pipeline(config_path):
    """Run a pipeline.

    Parameters
    ----------
    config_path: str
        Path to the config file with the pipeline definition.

    """
    pipeline = Pipeline(config_path)

    for step in pipeline.steps():
        logging.info('Running {}'.format(step.name))
        run_pipeline_step(step)


def parse_args():
    parser = argparse.ArgumentParser('Run the graphml module.')

    parser.add_argument('config', help='Path to the configuration file.')
    parser.add_argument('-v', '--verbose', help='Whether to log the inner '
                                                'workings of the module or '
                                                'not.')

    return parser.parse_args()


def main():
    args = parse_args()

    if args.verbose:
        logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    run_pipeline(args.config)


if __name__ == '__main__':
    main()
