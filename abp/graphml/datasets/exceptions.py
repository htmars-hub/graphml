# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.


class GraphSizeError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class AdjacencyAndFeaturesSizeMismatchError(GraphSizeError):
    MESSAGE_TEMPALTE = ('Adjacency and Feature matrices size mismatch for {'
                        '0}: Adjacency matrix has {1} nodes, while features '
                        'matrix has {2} nodes.')

    def __init__(self, file_path, adj_size, features_size):
        super().__init__(
            self.MESSAGE_TEMPALTE.format(file_path, adj_size, features_size),
        )


class WrongGraphSizeError(GraphSizeError):
    MESSAGE_TEMPLATE = (
        'Error while processing {0}: '
        'Invalid graph size: Got {1}, expected {2}.'
    )

    def __init__(self, file_path, graph_size, exp_graph_size):
        super().__init__(self.MESSAGE_TEMPLATE.format(
            file_path, graph_size, exp_graph_size,
        ))


class NotSquareAdjacencyMatrixError(GraphSizeError):
    MESSAGE_TEMPLATE = (
        'Error while processing {0}: Adjacency matrix expected to be square. '
        'Got a {1}x{2} matrix instead.'
    )

    def __init__(self, file_path, x, y):
        super().__init__(self.MESSAGE_TEMPLATE.format(file_path, x, y))


class FeaturesSizeError(Exception):
    MESSAGE_TEMPALTE = (
        'Error while processing {0}. Invalid features vector size: Got {1}, '
        'expected {2}.'
    )

    def __init__(self, file_path, features_count, exp_features_count):
        super().__init__(self.MESSAGE_TEMPALTE.format(
            file_path, features_count, exp_features_count,
        ))
