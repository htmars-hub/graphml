# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import numpy as np

from . import utils
from . import exceptions


class Dataset:
    """Class representing a loaded Dataset.

    Parameters
    ----------
    dataset_path: str or PosixPath
        Path to the directory where the dataset can be loaded from.

    Raises
    ------
    abp.graphml.datasets.exceptions.NotSquareAdjacencyMatrixError
        If, for any of the graphs that are loaded, the adjacency matrix is
        not square.
    abp.graphml.datasets.exceptions.AdjacencyAndFeaturesSizeMismatchError
        If, for any of the graphs the number of nodes in the adjacency
        matrix does not match the one for the features matrix.
    abp.graphml.datasets.exceptions.WrongGraphSizeError
        If any of the graphs has a different number of nodes than the
        others. In this case, the number of nodes of the first loaded graph
        is considered ground-truth.
    abp.graphml.datasets.exceptions.FeaturesSizeError
        If any of the graphs has a different number of features than the
        others. In this case, the number of features of the first loaded
        graph is considered ground-truth.

    """

    def __init__(self, dataset_path):
        self._data = {}
        self.nodes_count = None
        self.features_count = None
        self._size_checks_ok = True
        self.size = 0

        for file_path, contents in utils.get_all_graphs(dataset_path):
            # Performing checks
            self._check_graph_size(file_path, contents)
            self._check_features_size(file_path, contents)

            # Storing the graph
            self._data[file_path] = {
                'adjacency': contents[0],
                'features': contents[1],
                'depth': contents[3],
                'label': np.squeeze(contents[2]),
                'size': contents[0].shape[0],
            }

            self.size += 1

    def _check_graph_size(self, file_path, contents):
        if contents[0].shape[0] != contents[0].shape[1]:
            raise exceptions.NotSquareAdjacencyMatrixError(
                file_path, contents[0].shape[0], contents[0].shape[1],
            )

        nodes_count_adj = contents[0].shape[0]
        nodes_count_features = contents[1].shape[0]

        if nodes_count_adj != nodes_count_features:
            self._size_checks_ok = False
            raise exceptions.AdjacencyAndFeaturesSizeMismatchError(
                file_path, nodes_count_adj, nodes_count_features,
            )

        if self.nodes_count is None:
            self.nodes_count = nodes_count_adj

    def _check_features_size(self, file_path, contents):
        features_count = contents[1].shape[1]

        if self.features_count is None:
            self.features_count = features_count
        elif features_count != self.features_count:
            self._size_checks_ok = False
            raise exceptions.FeaturesSizeError(
                file_path, features_count, self.features_count,
            )

    def resize_graphs(self, target_size):
        """Resize all graphs to a specific size.

        If graphs are bigger than the expected size, they will be cropped.
        If they are smaller, they will be padded with no-ops (0s).

        Parameters
        ----------
        target_size: int
            The number all nodes all graphs are expected to have at the end.

        """
        for gid in self._data:
            self._data[gid]['adjacency'] = utils.resize_matrix(
                self._data[gid]['adjacency'],
                self._data[gid]['adjacency'].shape,
                (target_size, target_size),
            )
            self._data[gid]['features'] = utils.resize_matrix(
                self._data[gid]['features'],
                self._data[gid]['features'].shape,
                (target_size, self._data[gid]['features'].shape[1]),
            )

    def to_trainable(self, split=None, shuffle=False, include_depth=False):
        """Convert the dataset to a trainable format.

        Parameters
        ----------
        split: dict (str -> float)
            Mapping split names to the percentage of the dataset to be
            associated with them. If `None`, the entire dataset will be
            converted at once. Default `None`.
        shuffle: boolean
            Whether to shuffle the dataset before splitting or not. If
            `split` is `None`, this argument will be ignored.
        include_depth: boolean
            Whether to include the depth parameter in the returned result or
            not. Default False.

        Returns
        -------
        dict (str -> tuple)
            Mapping split names to their corresponding part of the dataset. If
            `split` is `None`, the dict will only have one key: `all`. Each
            tuple has the following format:

                (adjacency_matrix, feature_matrix, labels),

            if `include_depth` is set to False. Otherwise, the tuple will
            have this format:

                (adjacency_matrix, feature_matrix, labels, depths_vector)

        """
        if not self._size_checks_ok:
            raise Exception(
                'Cannot transform to trainable! There is a graph size error!'
            )
        all_files = list(self._data.keys())

        if split is None:
            split = {'all': 1}

        if abs(sum([v for _, v in split.items()]) - 1.0) > 1e-05:
            raise ValueError('Cannot split the dataset! The percentages do '
                             'not add up to 1!')

        if shuffle:
            all_files = np.array(all_files)
            np.random.shuffle(all_files)
            all_files = list(all_files)

        result = {}
        left_limit = 0
        right_limit = 0
        so_far = 0

        for split_name in split:
            left_limit = right_limit
            so_far += int(split[split_name] * self.size)
            right_limit = min(so_far, self.size)
            files_in_split = all_files[left_limit: right_limit]
            graphs_in_split = [self._data[f] for f in files_in_split]

            result[split_name] = utils.graphs_to_trainable(
                graphs_in_split, include_depth=include_depth,
            )

        return result
