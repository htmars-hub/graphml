# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pathlib
import logging

import joblib
import numpy as np


__all__ = ['get_all_graphs', 'graphs_to_trainable', 'resize_matrix']


def load(filename):
    """Load a pickled file.

    Parameters
    ----------
    filename: str
        Path to the file that needs to be loaded

    Returns
    -------
    str
        The loaded object.

    """
    try:
        return joblib.load(filename)
    except ValueError:
        import pickle
        with open(filename, 'rb') as f:
            return pickle.load(f, encoding='latin1')


def get_all_graphs(dataset_path):
    """Load all graphs from the dataset.

    Parameters
    ----------
    dataset_path: str
        Path to the dataset to load from.

    Yields
    -------
    file_path: str
        Path to the file that was loaded.
    data: tuple
        The contents of the file.

    """
    if not isinstance(dataset_path, pathlib.Path):
        dataset_path = pathlib.Path(dataset_path)

    all_files = sorted(list(dataset_path.glob('*.jl')))

    for fp in all_files:
        try:
            data = load(filename=fp)
            yield str(fp), data
        except Exception as err:
            logging.warning(f'Error loading {fp}: {err}')


def graphs_to_trainable(graphs, include_depth=False):
    """Convert a list of graphs to a trainable format.

    This method merges all adjacency, features, labels and depth matrices
    into a single numpy array for each.

    Parameters
    ----------
    graphs: iterable of dict
        An iterable with all the graphs that need to be converted to a
        trainable format. Each graph is expected to be a dictionary mapping
        strings (i.e. the name of the appropriate matrix - adjacency,
        features, label, depth) to the appropriate values.
    include_depth: boolean
        Optional flag marking whether the depth information should be
        included in the returned result or not. Default False.

    Returns
    -------
    A 3-tuple - if `include_depth` is set to False.
        With the following contents:
        (adjacency_matrices, features_matrices, labels_matrix)

    A 4-tuple - if `include_depth` is set to True
        With the following contents:
        (adjacency_matrices, features_matrices, labels_matrix, depth_matrix)

    """
    all_adj = [g['adjacency'] for g in graphs]
    all_features = [g['features'] for g in graphs]
    all_labels = [g['label'] for g in graphs]

    if include_depth:
        all_depths = [g['depth'] for g in graphs]

        return (
            np.array(all_adj), np.array(all_features), np.array(all_labels),
            np.array(all_depths),
        )

    return np.array(all_adj), np.array(all_features), np.array(all_labels)


def resize_matrix(matrix, matrix_shape, target_shape):
    """Resize a matrix (2D numpy array) to match a specific shape.

    This function first cuts off any axis that is longer than required and
    then pads them with 0s if necessary.

    Parameters
    ----------
    matrix: np.ndarray
        A 2-dimensional numpy array that needs to be resized.
    matrix_shape: tuple
        A tuple representing the original of the input matrix
    target_shape: tuple
        A tuple representing the target shape of the resulting matrix.

    Returns
    -------
    np.ndarray
        The 2-dimensional array resulting from the resizing operation.

    """
    if matrix_shape == target_shape:
        # No resizing required
        return matrix

    if (matrix_shape[0] >= target_shape[0] and
            matrix_shape[1] >= target_shape[1]):
        # We only need to cutoff
        return np.resize(matrix, target_shape)

    if matrix_shape[0] >= target_shape[0]:
        matrix = np.resize(matrix, (target_shape[0], matrix_shape[1]))

    if matrix_shape[1] >= target_shape[1]:
        matrix = np.resize(matrix, (matrix_shape[0], target_shape[1]))

    # Padding to the correct size
    left_to_pad_lines = target_shape[0] - matrix.shape[0]
    left_to_pad_cols = target_shape[1] - matrix.shape[1]

    return np.pad(
        matrix, ((0, left_to_pad_lines), (0, left_to_pad_cols)),
    )
