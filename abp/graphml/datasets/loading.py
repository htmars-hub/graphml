# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

from . import dataset


def load_converted_dataset(path):
    """Load a pre-converted dataset.

    This function is expected to work with datasets where each graph is
    saved individually, as a pickled `.jl` file.

    Parameters
    ----------
    path: str or PosixPath
        Path to the converted dataset that needs to be loaded.

    Returns
    -------
    abp.graphml.datasets.dataset.Dataset
        With the contents of the dataset.

    """
    return dataset.Dataset(path)
