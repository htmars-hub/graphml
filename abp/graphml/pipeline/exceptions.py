# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.


class PipelineError(Exception):

    def __init__(self, desc):
        if isinstance(desc, StepError):
            super(PipelineError, self).__init__(
                'Step {} failed: {}'.format(desc.name, desc.name),
            )
        else:
            super(PipelineError, self).__init__(desc)


class StepError(Exception):

    def __init__(self, msg, name):
        super(StepError, self).__init__(msg)
        self.msg = msg
        self.name = name

    def __str__(self):
        return 'Step {} failed: {}'.format(self.name, self.msg)
