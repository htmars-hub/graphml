# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.
import abp.graphml.datasets as datasets
import abp.graphml.models as models
from . import constants as const
from . import exceptions as err
import abp.graphml.models.actions as model_actions
from . import utils


__all__ = ['run_pipeline_step']


def run_pipeline_step(step):
    """Run a pipeline step.

    Parameters
    ----------
    step: abp.graphml.pipeline.pipeline.PipelineStep
       The pipeline step that will be run.

    """
    if step.action == 'train':
        _run_train(step)
    elif step.action == 'eval':
        _run_eval(step)


def _validate_attributes(step, exp_attributes):
    actual_attributes = set(step.__dict__.keys())

    if not exp_attributes.issubset(actual_attributes):
        raise err.StepError(
            step.name,
            'Missing mandatory attribute(s): {}'.format(
                exp_attributes.difference(actual_attributes),
            ),
        )


def _get_converted_dataset(step):
    dataset_dir = step.get_attribute('dataset_path')

    return datasets.loading.load_converted_dataset(dataset_dir)


def _load_model(step, hyperparameters, load_weights=False):
    use_depth = utils.get_attr_as_bool(step, 'use_depth')
    model_name = step.get_attribute('model')

    model = models.CLASSIFIERS[model_name](hyperparameters, use_depth)

    if load_weights:
        model.load_weights(step.model_save_path)

    return model_name, model


def _run_train(step):
    _validate_attributes(step, const.MandatoryAttributes.TRAIN)

    use_depth = utils.get_attr_as_bool(step, 'use_depth')
    nodes_count = step.get_attribute('nodes', 100)
    dataset = _get_converted_dataset(step)
    dataset.resize_graphs(nodes_count)
    trainable_dataset = dataset.to_trainable(include_depth=use_depth)['all']

    print(trainable_dataset[-1])

    hyperparameters = utils.extract_hyperparams_from_step(step)
    hyperparameters.add('features', dataset.features_count)
    hyperparameters.add('nodes', nodes_count)

    model_name, model = _load_model(step, hyperparameters)

    model_export_path = step.get_attribute('model_export_path')
    model_save_to = step.get_attribute('model_save_path')
    save_best_only = utils.get_attr_as_bool(
        step, 'save_best_only', default=True)
    checkpoints_path = step.get_attribute('checkpoints_path')

    result = model_actions.TRAIN_HANDLERS[model_name](
        model, trainable_dataset, hyperparameters, model_save_to,
        model_export_path, save_best_only=save_best_only,
        checkpoints_dir=checkpoints_path,
        use_depth=use_depth,
    )

    return result


def _run_eval(step):
    _validate_attributes(step, const.MandatoryAttributes.TRAIN)

    nodes_count = step.get_attribute('nodes', 100)
    dataset = _get_converted_dataset(step)
    dataset.resize_graphs(nodes_count)

    hyperparameters = utils.extract_hyperparams_from_step(step)
    hyperparameters.add('features', dataset.features_count)
    hyperparameters.add('nodes', nodes_count)

    if utils.get_attr_as_bool(step, 'retrain', default=True):
        model_name, model = _load_model(step,
                                        hyperparameters, load_weights=False)
        test_split = step.get_attribute('test_split', .1)
        trainable_dataset = dataset.to_trainable(
            split={'train': 1 - test_split, 'test': test_split},
            include_depth=utils.get_attr_as_bool(step, 'use_depth'),
        )
        test_samples = trainable_dataset['test'][0].shape[0]
        test_data = trainable_dataset['test']
        train_data = trainable_dataset['train']
        result = model_actions.EVAL_HANDLERS[model_name](
            model, test_data, hyperparameters, train_data=train_data,
        )
    else:
        model_name, model = _load_model(step,
                                        hyperparameters, load_weights=True)
        trainable_dataset = dataset.to_trainable(
            include_depth=utils.get_attr_as_bool(step, 'use_depth'),
        )['all']
        test_samples = trainable_dataset[0].shape[0]
        result = model_actions.EVAL_HANDLERS[model_name](
            model, trainable_dataset, hyperparameters,
        )

    report = const.EVAL_REPORT_TEMPLATE.format(
        step.name, test_samples,
        '\n'.join([const.EVAL_RESULT_TEMPLATE.format(key, value)
                   for key, value in result.items()]),
    )

    print(report)

    return result
