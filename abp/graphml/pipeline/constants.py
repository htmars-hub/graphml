# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.


CONFIG_OPTION_PROCESSING_FUNC = {
    'steps': lambda x: x.split(','),
    'test_split': lambda x: float(x),
    'nodes': lambda x: int(x),
    'features': lambda x: int(x),
    'save_best_only': lambda x: bool(x),
}


class MandatoryAttributes:
    TRAIN = {'dataset_path', 'data_type', 'model', 'nodes', 'features',
             'checkpoints_path'}
    EVAL = {'dataset_path', 'data_type', 'model', 'nodes', 'test_split',
            'features'}


EVAL_REPORT_TEMPLATE = '''
======= Evaluation report =======
Model: {}
Test samples: {}
============ Results ============
{}
'''

EVAL_RESULT_TEMPLATE = '{}: {}'
