# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import configparser
import os
import sys

from . import constants as const
from . import exceptions as err


def _validate_raw_config(raw_cnf, section, mandatory_opts=set()):
    actual_options = set(raw_cnf.options(section))
    if not mandatory_opts.issubset(actual_options):
        raise err.StepError(
            'Mandatory options missing in config: {}'.format(
                mandatory_opts.difference(actual_options),
            ),
            section,
        )


def _process_item(key, value):
    if key in const.CONFIG_OPTION_PROCESSING_FUNC:
        return const.CONFIG_OPTION_PROCESSING_FUNC[key](value)

    return value


class Pipeline:
    """Class representing a module Pipeline.

    The pipeline can be made up of one or more steps.

    Parameters
    ----------
    config_path: str
        Path to the configuration file that will be used when setting up the
        pipeline.

    """

    INVALID_DEFAULTS = {'action'}
    _MANDATORY_MAIN_OPTIONS = {'steps'}

    def __init__(self, config_path):
        self.step_names = []
        self.defaults = {}

        self.raw_cnf = self._read_raw_config(config_path)
        self._process_conf_main_section()

    def _read_raw_config(self, path):
        raw_cnf = configparser.ConfigParser()

        if not os.path.exists(path):
            sys.exit('No config could be found at {}'.format(path))

        with open(path, 'rt', encoding='utf-8') as f:
            raw_cnf.read_file(f)

        return raw_cnf

    def _process_conf_main_section(self):
        _validate_raw_config(self.raw_cnf, 'main',
                             self._MANDATORY_MAIN_OPTIONS)
        steps = self.raw_cnf.get('main', 'steps')
        self.step_names = const.CONFIG_OPTION_PROCESSING_FUNC['steps'](steps)

        default_names = [x for x in self.raw_cnf.options('main')
                         if x != 'steps']

        for default in default_names:
            self.defaults[default] = _process_item(
                default, self.raw_cnf.get('main', default),
            )

        self._validate_defaults()

    def _validate_defaults(self):
        default_names = set(self.defaults.keys())

        if not self.INVALID_DEFAULTS.isdisjoint(default_names):
            raise err.PipelineError(
                'There are invalid values in the default section of the '
                'config file: {}'.format(
                    self.INVALID_DEFAULTS.intersection(default_names),
                ),
            )

        if any([name.startswith('_') for name in default_names]):
            raise err.PipelineError(
                'Option names that start with "_" are not permitted!',
            )

    def steps(self):
        """Get each pipeline step as a separate object.

        Yields
        ------
        abp.graphml.pipeline.PipelineStep
            With the appropriate configuration.

        """
        print(self.step_names)
        for name in self.step_names:
            yield PipelineStep(self.raw_cnf, name, self.defaults)


class PipelineStep:
    """Class representing a step in the module pipeline.

    Parameters
    ----------
    raw_conf: configparser.ConfigParser
        With the raw config data.
    name: str
        The name of the pipeline step.

    """

    _MANDATORY_OPTIONS = {'action'}
    _INVALID_OPTIONS = set()

    def __init__(self, raw_conf, name, defaults):
        _validate_raw_config(raw_conf, name, self._MANDATORY_OPTIONS)
        self.name = name
        self._raw_conf = raw_conf
        self._add_defaults(defaults)
        self._parse_config_section()

    def _add_defaults(self, defaults):
        for option in defaults:
            self.__setattr__(option, defaults[option])

    def _check_option(self, option):
        if option in self._INVALID_OPTIONS:
            raise err.StepError(
                'Illegal option name: "{}"'.format(option), self.name
            )

        if option.startswith('_'):
            raise err.StepError(
                'Option names are not allowed to start with "_": "{}"'.format(
                    option
                ), self.name,
            )

    def _parse_config_section(self):
        all_options = set(self._raw_conf.options(self.name))

        for option in all_options:
            self._check_option(option)
            self.__setattr__(
                option,
                _process_item(option, self._raw_conf.get(self.name, option)),
            )

    def get_attribute(self, name, default=None):
        """Get an attribute by name.

        Parameters
        ----------
        name: str
            The name of the attribute we're looking for.
        default: Union[object, None]
            The default value for the attribute, which will be returned if
            it couldn't be found. Default `None`
        Returns
        -------
        The resulting attribute value.

        """
        try:
            return self.__getattribute__(name)
        except AttributeError:
            return default
