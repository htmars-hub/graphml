# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not

import logging

import abp.graphml.models.constants as model_const
import abp.graphml.models.utils as model_utils


TRUTHY_VALUES = {'yes', 'Yes', 'true', 'True', '1'}


def get_attr_as_bool(step, attr, default=False):
    use_depth_str = str(step.get_attribute(attr, default))
    return use_depth_str in TRUTHY_VALUES


def extract_hyperparams_from_step(step):
    """Extract the hyperparameters from the step configuration.

    Parameters
    ----------
    step: abp.graphml.pipeline.pipeline.PipelineStep
        The step we want to extract the hyperparameters from.

    Returns
    -------
    HyperParameters
        The extracted hyperparams.

    """
    hyperparams = model_utils.HyperParameters()

    for name in model_const.HYPERPARAMS_TYPES:
        value = step.get_attribute(name)

        if value is None:
            continue
        try:
            # Casting the value to the appropriate data type
            value = model_const.HYPERPARAMS_TYPES[name](value)
            hyperparams.add(name, value)
        except Exception as err:
            logging.warning(
                'Could not cast hyperparameter "{}" to "{}". Complete error '
                'message: {}'.format(
                    name, model_const.HYPERPARAMS_TYPES[name], err,
                ),
            )

    return hyperparams
