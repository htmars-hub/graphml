## Graphs classifiers

### Prerequisites:

* Python 3.5+
* graphviz-dev
* libpython3-dev


### Setting up a pipeline

This module is thought out to be as modular as possible, and therefore setting
up a brand new pipeline is extremely easy, using a `.ini` file.

An example configuration file would be:

```ini
[main]
steps = step1,step2,step1,step3
some_parameter = foo
other_parameter = bar

[step1]
action = train
one_more_parameter = baz

[step2]
action = evaluate
some_parameter = faa

[step3]
action = deploy
another_param = bam
```

The `main` section has one mandatory option:
- `steps`: which can be used to specify the pipeline steps as comma-separated values.

The other options are then considered to be defaults for every step-specific section.

The following option names are not allowed in `main` and the entire program
will fail if they are encountered:

1. `action`.
2. `name`.
3. Any option name starting with `_`.

It is **mandatory** for each section to have its own section, with at least one
option: `action`, which specifies what that specific step is suppose to do.
The currently supported actions are: `train`, `predict`, `eval` and
`convert_dataset`. As mentioned earlier, each step-specific section inherits the
default options defined in `main`, but can, on top of that, define new options
(see `step1` and `step3` in the example above) or even overwrite the defaults
(see `step2`).

The following options names are not allowed within step-specific sections and
will cause the program to fail running that step:

1. `name`.
2. Any option name starting with `_`.

|     Action     |     Description    | More details |
|:--------------:|:----------:|:----------:|
|    train    | Train a model and (potentially) save it in multiple formats.|  [train.md](docs/train.md) |
|    eval    |  Run a number of evaluation steps against a model (pre-trained or not).  |  Soon |
|    predict      |    Run a set of predictions against a model (pre-trained or not).   |  Soon |
|    convert_dataset |   Convert a dataset | Soon |



### Running a pipeline

If you have the package installed, you can simply run:

```bash
graphml <path to the config file>
```

As an alternative, you can run it as a python script:
```bash
python -m abp.graphml.run <path to the config file>
```
### Running with docker

You can run training and conversion of model with
tfjs using docker.
To make use of the `Dockerfile`, run the following from the project root:
```bash
docker build -t <image name> .
```

To run the docker container:
```bash
docker run --name <container name>
 --mount type=bind,source=<path to the data folder>,target=/data,readonly
 --mount type=bind,source=<path to the model output>,target=/models
 --mount type=bind,source=<path to the config folder>,target=/configs,readonly
<image name>
```

### Model Versions

Similar to `MAJOR.MINOR.PATCH` versions that are used in software packages, where:

- MAJOR: signifies presence of incompatible changes (e.g. API changes).
- MINOR: adds functionality in backward compatible manner.
- PATCH: makes backward compatible bug fixes.

we use `MAJOR.MINOR.PIPELINE` for ML model versions, where:

- MAJOR: signifies incompatibility of the new model with the previous one
(e.g. incompatibility in data schema or target variable, loss function,
model architecture, etc.)
- MINOR: denotes improvements in model performance.
- PIPELINE: denotes other changes or fixes in the training pipeline of the model which
may or may not improve or change the model itself.
