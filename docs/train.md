## Training step

### Using the pipeline config

The following options are required to be present in any step-specific section
with `action = train`.

|     Option     |     Description    | Allowed values  |
|:--------------:|:------------------:|:---------------:|
| dataset_path   |   Path to the dataset that will be used in training | Any valid path |
| data_type      | The type of the data we're working with - to understand what preprocessing steps are required| `raw` |
| model |  The name of the model that will be trained | `graph_conv_net` |
| nodes | Number of nodes to be considered in each graph | Any positive integer. |


The following options are not required, but can be used in this context:

|     Option     |     Description    | Allowed values  |
|:--------------:|:------------------:|:---------------:|
| model_dir   |   Model directory for an estimator. Tensorboard info and checkpoints etc will be written there | Any valid path to a directory |
| model_export_dir|Path to a directory where the model should be exported to, in a `tf.js`-readable format.| Any valid path to a directory |
| model_save_path |  Path to the file where the model will be saved in `keras`-specific format. |Any valid path to a `.h5` file.|
