#!/bin/bash

display_help() {
    echo "Usage: $0" >&2
    echo
    echo "   -h, --help     Print this message."
    echo "   -c, --config  Config file name"
    echo

    exit 1
}

function main() {

    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        -h|--help)
        display_help
        ;;
        -c|--config)
        CONFIG="$2"
        shift
        shift
        ;;
        *)    # unknown option
        shift # past argument
        ;;
    esac
    done

    if [ -z "$CONFIG" ]
    then
        echo "Config file name is a required argument"
        echo "---"
        display_help
    fi


    graphml /configs/"$CONFIG"
    python convert.py -i /models/exported -o /models/
}

main "$@"


