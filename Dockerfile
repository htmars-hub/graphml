# Pull base image.
FROM python:3.7.9-slim

RUN apt update
RUN pip install --upgrade pip
RUN apt-get install gcc -y

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

RUN pip install .

RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
