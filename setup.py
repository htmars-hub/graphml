# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

from setuptools import setup

DESCRIPTION = 'Graph-based ML approach to adblocking.'
LONG_DESCR = DESCRIPTION

INSTALL_REQUIRES = ['networkx', 'numpy', 'tensorflow==2.2.0',
                    'spektral', 'matplotlib', 'lxml',
                    'orjson', 'h5py<3.0.0']


if __name__ == '__main__':
    setup(
        name='abp.graphml',
        version='0.0.1',
        description=DESCRIPTION,
        long_description=LONG_DESCR,
        url='https://gitlab.com/eyeo/sandbox/graphml/',
        author='eyeo GmbH',
        author_email='info@eyeo.com',
        license='GPLv3',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Intended Audience :: ML Engineers/ Data Scientists',
            'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
            'Programming Language :: Python :: 3',
        ],
        keywords='adblocking machine-learning classifier graphs',
        packages=['abp.graphml', 'abp.graphml.datasets',
                  'abp.graphml.models', 'abp.graphml.models.actions',
                  'abp.graphml.pipeline'],
        entry_points={
            'console_scripts': ['graphml=abp.graphml.run:main']
        },
        install_requires=INSTALL_REQUIRES,
    )
