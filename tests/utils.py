# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import os

import tensorflow.keras as keras

import abp.graphml.models.constants as const
import abp.graphml.models.utils as models_utils


class MockKerasClassifier:

    def __init__(self, logs_file, const_class=const.ModelConstants,
                 use_depth=False, hyperparameters=None):
        self.input = ['Adjacency input', 'Features input']
        if use_depth:
            self.input += ['Depth input']
        self.output = 'Output layer'
        self.logs_file = logs_file
        self.const_class = const_class
        self.kwargs_train = {}
        self._setup_kwargs_to_check(hyperparameters)

    def _setup_kwargs_to_check(self, hyperparameters):
        if hyperparameters is None:
            hyperparameters = models_utils.HyperParameters()

        self.kwargs_train['epochs'] = hyperparameters.get(
            'epochs', self.const_class.EPOCHS,
        )
        self.kwargs_train['callbacks'] = self.const_class.TRAINING_CALLBACKS
        self.kwargs_train['validation_split'] = hyperparameters.get(
            'validation_split', self.const_class.VALIDATION_SPLIT
        )
        self.kwargs_train['batch_size'] = hyperparameters.get(
            'batch_size', self.const_class.BATCH_SIZE
        )

    def _ad_log(self, text):
        mode = 'a'
        if not os.path.exists(self.logs_file):
            mode = 'w'

        with open(self.logs_file, mode) as stream:
            stream.write('{}\n'.format(text))

    def fit(self, *args, **kwargs):
        assert len(args) == 2
        assert len(args[0]) == 2
        assert args[0][0].shape[0] == args[0][1].shape[0] == args[1].shape[0]

        assert self.kwargs_train == kwargs

        self._ad_log('fit')

    def save(self, path):
        with open(path, 'w') as stream:
            stream.write('I was saved!')

    def predict(self, *args, **kwargs):
        assert len(args) == 1
        assert len(args[0]) == 2
        assert args[0][0].shape[0] == args[0][1].shape[0]

        self._ad_log('predict - verbose={}'.format(kwargs.get('verbose')))

    def predict_proba(self, *args, **kwargs):
        assert len(args) == 1
        assert len(args[0]) == 2
        assert args[0][0].shape[0] == args[0][1].shape[0]

        self._ad_log('predict_proba - verbose={}'.format(kwargs.get(
            'verbose')))

    def evaluate(self, *args, **kwargs):
        assert len(args) == 2
        assert len(args[0]) == 2
        assert args[0][0].shape[0] == args[0][1].shape[0] == args[1].shape[0]

        assert kwargs['batch_size'] == self.const_class.BATCH_SIZE

        result = [.1]

        for _ in self.const_class.METRICS:
            result.append(.1)

        self._ad_log('eval')

        return result


class MockOptimizer(keras.optimizers.Optimizer):
    def __init__(self, lr, decay, *args, **kwargs):
        super().__init__(name='MockOptimiser')
        self.learning_rate = lr
        self.decay = decay

    def get_config(self):
        pass


def mock_eval_fn(*args):
    return .1
