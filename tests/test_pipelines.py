# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest


import abp.graphml.pipeline as pipeline
from abp.graphml.pipeline.pipeline import PipelineStep
import abp.graphml.pipeline.utils as utils


def test_pipeline_init_valid(configs_dir):
    config_path = configs_dir.join('valid.ini')
    exp_defaults = {'nodes': 10, 'test_split': .1, 'data_path': 'baz'}
    exp_step_values = [
        {'name': 'foo', 'nodes': 10, 'test_split': .1, 'data_path': 'baz',
         'action': 'train', 'model_name': 'graph_conv_net'},
        {'name': 'bar', 'nodes': 40, 'test_split': .1, 'data_path': 'faz',
         'action': 'predict', 'model_name': 'graph_conv_net'}
    ]

    p = pipeline.Pipeline(str(config_path))
    assert p.defaults == exp_defaults
    steps = p.steps()
    idx = 0

    for step in steps:
        assert isinstance(step, PipelineStep)
        for key, value in exp_step_values[idx].items():
            assert step.__getattribute__(key) == value
        idx += 1


@pytest.mark.parametrize('cnf_id,exp_err,exp_msg', [
    (1, pipeline.exceptions.PipelineError,
     'Option names that start with "_" are not permitted!'),
    (2, pipeline.exceptions.PipelineError,
     'There are invalid values in the default section of the '
     "config file: {'action'}"),
    (3, pipeline.exceptions.StepError,
     'Option names are not allowed to start with "_": "_faz"'),
])
def test_pipeline_init_invalid_names(cnf_id, exp_err, exp_msg, configs_dir):
    cnf_path = configs_dir.join('invalid_names_{}.ini'.format(cnf_id))

    with pytest.raises(exp_err) as err:
        p = pipeline.Pipeline(str(cnf_path))
        for _ in p.steps():
            continue

    assert exp_msg in str(err.value)


def test_pipeline_init_no_action(configs_dir):
    cnf_path = configs_dir.join('invalid_no_action.ini')
    exp_msg = "Mandatory options missing in config: {'action'}"

    with pytest.raises(pipeline.exceptions.StepError) as err:
        p = pipeline.Pipeline(str(cnf_path))
        for _ in p.steps():
            continue

    assert exp_msg in str(err.value)


def test_load_hyperparameters_from_step(configs_dir):
    p = pipeline.Pipeline(str(configs_dir.join('hyperparameters.ini')))

    step = list(p.steps())[0]

    hyperparams = utils.extract_hyperparams_from_step(step)

    assert hyperparams.learning_rate == 1e-10
    assert hyperparams.epochs == 500
    assert hyperparams.nodes == 10
    assert hyperparams.armaconv_dropout == 0.10
    assert hyperparams.features_dropout == 0.10
