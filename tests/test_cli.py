# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest

import abp.graphml.models.constants as const


@pytest.mark.slow
@pytest.mark.parametrize('depth_flag', ['no_depth', 'use_depth'])
@pytest.mark.script_launch_mode('inprocess')
def test_train(script_runner, monkeypatch, depth_flag,
               configs_dir, valid_data_dir, tmpdir):
    monkeypatch.setattr(const.GraphConvNet, 'EPOCHS', 1)
    monkeypatch.setattr(const.GraphConvNet, 'BATCH_SIZE', 1)

    config_template = configs_dir.join('train.ini.tmpl').read()
    config = config_template.format(
        dataset_path=valid_data_dir,
        checkpoints_path=tmpdir.join('checkpoints'),
        model_save_path=tmpdir.join('model.h5'),
        model_export_path=tmpdir.join('export'),
        depth_flag=depth_flag,
    )
    config_path = tmpdir.join('config.ini')
    config_path.write(config)

    result = script_runner.run('graphml', str(config_path))

    assert result.success
