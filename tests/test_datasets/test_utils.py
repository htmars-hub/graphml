# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import types

import numpy as np
import pytest

import abp.graphml.datasets.utils as utils


def test_get_all_graphs(valid_data_dir):
    data = utils.get_all_graphs(valid_data_dir)
    assert isinstance(data, types.GeneratorType)
    data = list(data)
    assert len(data) == 2

    exp_data = [
        ('tests/data/valid/graph1.jl',
         (np.array([[1, 0], [1, 0]]), np.array([[1, 2, 3], [1, 2, 3]]),
          np.array([1, 0]), 10),
         ),
        ('tests/data/valid/graph2.jl',
         (np.array([[1, 0], [1, 0]]), np.array([[1, 2, 3], [1, 2, 3]]),
          np.array([0, 1]), 20),
         )
    ]

    for read, exp in zip(data, exp_data):
        assert read[0].endswith(exp[0])
        np.testing.assert_equal(read[1][0], exp[1][0])
        np.testing.assert_equal(read[1][1], exp[1][1])
        np.testing.assert_equal(read[1][2], exp[1][2])
        assert read[1][3] == exp[1][3]


@pytest.mark.parametrize('include_depth', [True, False])
def test_graphs_to_trainable(include_depth):
    graph1 = {
        'adjacency': np.array([[0, 1], [1, 0]]),
        'features': np.array([[1, 2, 3], [1, 2, 3]]),
        'label': np.array([0, 1]),
        'depth': 10,
    }

    graph2 = {
        'adjacency': np.array([[1, 0], [0, 1]]),
        'features': np.array([[1, 2, 3], [1, 2, 3]]),
        'label': np.array([1, 0]),
        'depth': 20,
    }

    result = utils.graphs_to_trainable([graph1, graph2], include_depth)

    if include_depth:
        assert len(result) == 4
        np.testing.assert_equal(result[3], np.array([10, 20]))
    else:
        assert len(result) == 3

    np.testing.assert_equal(
        result[0], np.array([[[0, 1], [1, 0]], [[1, 0], [0, 1]]]),
    )
    np.testing.assert_equal(
        result[1], np.array([[[1, 2, 3], [1, 2, 3]], [[1, 2, 3], [1, 2, 3]]]),
    )
    np.testing.assert_equal(result[2], np.array([[0, 1], [1, 0]]))


@pytest.mark.parametrize('target_shape,exp_out', [
    ((2, 2), np.array([[1, 0], [0, 1]])),
    ((1, 1), np.array([[1]])),
    ((1, 2), np.array([[1, 0]])),
    ((2, 1), np.array([[1], [0]])),
    ((2, 3), np.array([[1, 0, 0], [0, 1, 0]])),
    ((3, 2), np.array([[1, 0], [0, 1], [0, 0]])),
    ((3, 3), np.array([[1, 0, 0], [0, 1, 0], [0, 0, 0]])),
])
def test_resize_matrix(target_shape, exp_out):
    input_matrix = np.array([[1, 0], [0, 1]])

    output = utils.resize_matrix(input_matrix, input_matrix.shape,
                                 target_shape)

    assert output.shape == target_shape
    np.testing.assert_equal(output, exp_out)
