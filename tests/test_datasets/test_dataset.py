# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest
import numpy as np

import abp.graphml.datasets.dataset as dataset
import abp.graphml.datasets.exceptions as errs


def test_constructor(valid_data_dir):
    loaded_dataset = dataset.Dataset(valid_data_dir)

    assert loaded_dataset.size == 2
    assert loaded_dataset.nodes_count == 2
    assert loaded_dataset.features_count == 3


@pytest.mark.parametrize('directory,exp_error,exp_msg', [
    ('adj_not_square', errs.NotSquareAdjacencyMatrixError,
     'Adjacency matrix expected to be square. Got a 1x2 matrix instead.'),
    ('features_size_error', errs.FeaturesSizeError,
     'Invalid features vector size: Got 2, expected 3.'),
    ('size_mismatch_error', errs.AdjacencyAndFeaturesSizeMismatchError,
     'Adjacency matrix has 2 nodes, while features matrix has 1 nodes.'),
])
def test_adj_not_square_error(invalid_data_dir, directory, exp_error, exp_msg):
    dataset_path = invalid_data_dir.join(directory)

    with pytest.raises(exp_error) as err:
        _ = dataset.Dataset(dataset_path)

    error_msg = str(err.value)
    assert error_msg.endswith(exp_msg)


@pytest.mark.parametrize('split,include_depth,exp_output', [
    (None, True,
     {'all': (np.array([[[1, 0], [1, 0]], [[1, 0], [1, 0]]]),
              np.array([[[1, 2, 3], [1, 2, 3]], [[1, 2, 3], [1, 2, 3]]]),
              np.array([[1, 0], [0, 1]]),
              np.array([10, 20]))}),
    (None, False,
     {'all': (np.array([[[1, 0], [1, 0]], [[1, 0], [1, 0]]]),
              np.array([[[1, 2, 3], [1, 2, 3]], [[1, 2, 3], [1, 2, 3]]]),
              np.array([[1, 0], [0, 1]]))}),
    ({'first': 0.5, 'second': 0.5}, False, {
        'first': (np.array([[[1, 0], [1, 0]]]),
                  np.array([[[1, 2, 3], [1, 2, 3]]]), np.array([[1, 0]])),
        'second': (np.array([[[1, 0], [1, 0]]]),
                   np.array([[[1, 2, 3], [1, 2, 3]]]), np.array([[0, 1]])),
    }),
])
def test_to_trainable(valid_data_dir, split, include_depth, exp_output):
    ds = dataset.Dataset(valid_data_dir)

    result = ds.to_trainable(split=split, include_depth=include_depth)

    assert isinstance(result, dict)
    assert list(result.keys()) == list(exp_output.keys())

    for key in result:
        res_split = result[key]
        exp_split = exp_output[key]
        np.testing.assert_equal(res_split[0], exp_split[0])
        np.testing.assert_equal(res_split[1], exp_split[1])
        np.testing.assert_equal(res_split[2], exp_split[2])

        if include_depth:
            assert len(res_split) == 4
            np.testing.assert_equal(res_split[3], exp_split[3])
        else:
            assert len(exp_split) == 3


@pytest.mark.parametrize('target_size,exp_shape_adj,exp_shape_features', [
    (5, (5, 5), (5, 3)),
    (1, (1, 1), (1, 3)),
])
def test_resize_graphs(valid_data_dir, target_size, exp_shape_adj,
                       exp_shape_features):
    ds = dataset.Dataset(valid_data_dir)
    ds.resize_graphs(target_size)

    for graph in ds._data.values():
        assert graph['adjacency'].shape == exp_shape_adj
        assert graph['features'].shape == exp_shape_features
