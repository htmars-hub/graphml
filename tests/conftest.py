# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import os

import py
import pytest

DATA_PATH = py.path.local(__file__).dirpath('data')
CONFIGS_PATH = py.path.local(__file__).dirpath('configs')
MODELS_PATH = py.path.local(__file__).dirpath('models')


@pytest.fixture(scope='session')
def data_dir():
    return DATA_PATH


@pytest.fixture(scope='session')
def invalid_data_dir(data_dir):
    return data_dir.join('invalid')


@pytest.fixture(scope='session')
def valid_data_dir(data_dir):
    return data_dir.join('valid')


@pytest.fixture(scope='session')
def configs_dir():
    return CONFIGS_PATH


@pytest.fixture()
def outdir(tmpdir):
    outdir_path = tmpdir.join('outdir')
    if not os.path.exists(str(outdir_path)):
        os.mkdir(str(outdir_path))
    return tmpdir.join('outdir')


@pytest.fixture(scope='session')
def models_dir():
    return MODELS_PATH
