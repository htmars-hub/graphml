# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest
from tensorflow.keras import backend as K
import tensorflow.keras as keras

from abp.graphml.models import CLASSIFIERS
from abp.graphml.models.utils import HyperParameters
from tests.utils import MockOptimizer


@pytest.mark.parametrize('model,args,kwargs', [
    ('foo', ['bam', 'bar', 'baz'], {'la': 'lala'}),
])
def test_unsupported_handlers(model, args, kwargs):
    with pytest.raises(Exception) as err:
        CLASSIFIERS[model](*args, **kwargs)
    assert 'Unsupported model in config!' in str(err.value)


@pytest.mark.parametrize('model_name,hyperparams,kwargs,exp_out_type', [
    ('graph_conv_net', HyperParameters(), {}, keras.Model),
])
def test_supported_handlers(model_name, hyperparams, kwargs, exp_out_type):
    hyperparams.add('nodes', 10)
    hyperparams.add('features', 10)
    try:
        model = CLASSIFIERS[model_name](hyperparams, **kwargs)
        assert isinstance(model, exp_out_type)

    finally:
        K.clear_session()


def test_hyperparameters_parsed_correctly(monkeypatch):
    monkeypatch.setattr(keras.optimizers, 'Adam', MockOptimizer)

    hyperparameters = HyperParameters()
    hyperparameters.add('nodes', 10)
    hyperparameters.add('epochs', 50)
    hyperparameters.add('features', 66)
    hyperparameters.add('learning_rate', .10)
    hyperparameters.add('optimizer_decay', 1e-10)

    model = CLASSIFIERS['graph_conv_net'](hyperparameters)

    assert model.optimizer is not None
    assert model.optimizer.learning_rate == .10
    assert model.optimizer.decay == 1e-10
