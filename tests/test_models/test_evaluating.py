# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest
import numpy as np

from abp.graphml.models.actions import EVAL_HANDLERS
import abp.graphml.models.constants as const
from abp.graphml.models.utils import HyperParameters
from tests.utils import MockKerasClassifier, mock_eval_fn


def test_unsupported_models():
    exp_msg = 'Evaluating for this model is not currently supported.'
    with pytest.raises(Exception) as err:
        EVAL_HANDLERS['foo']('bar', 'baz', faz='ber')

    assert exp_msg in str(err.value)


@pytest.mark.parametrize('train_data,eval_fns,exp_logs', [
    (None, None, ['eval']),
    ([np.array([1]), np.array([0]), np.array([[0, 1]])], None,
     ['fit', 'eval']),
    (None, {'m1': mock_eval_fn, 'm2': mock_eval_fn},
     ['eval', 'predict - verbose=0']),
    ([np.array([1]), np.array([0]), np.array([[0, 1]])],
     {'m1': mock_eval_fn, 'm2': mock_eval_fn},
     ['fit', 'eval', 'predict - verbose=0']),

])
def test_graph_conv_net(train_data, eval_fns, exp_logs, outdir):
    exp_result = {'loss': .1}
    hyperparams = HyperParameters()

    for m in const.GraphConvNet.METRICS:
        exp_result[m] = .1

    if eval_fns:
        for key in eval_fns:
            exp_result[key] = .1

    logs_file = outdir.join('logs.txt')
    model = MockKerasClassifier(str(logs_file), const.GraphConvNet)

    data_in = [
        np.array([[1, 2, 3], [3, 4, 5]]),
        np.array([[0, 0, 0], [1, 1, 1]]),
        np.array([[0, 1], [1, 0]]),
    ]

    result = EVAL_HANDLERS['graph_conv_net'](model, data_in, hyperparams,
                                             train_data, eval_fns)

    assert result == exp_result
    assert exp_logs == logs_file.read().split('\n')[:-1]


@pytest.mark.parametrize('data_in', [[1, 2], [1, 2, 3, 4]])
def test_graph_conv_net_invalid_data_in(data_in):
    hyperparams = HyperParameters()
    exp_msg = ('The data parameter must have exactly 3 arguments. Got {}. '
               'Check the docstring for further '
               'information.'.format(len(data_in)))

    with pytest.raises(ValueError) as err:
        EVAL_HANDLERS['graph_conv_net']('foo', data_in, hyperparams)

    assert exp_msg in str(err.value)
