# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest
import numpy as np

from abp.graphml.models.actions import TRAIN_HANDLERS
from tests.utils import MockKerasClassifier
from abp.graphml.models import constants as const
from abp.graphml.models import utils as models_utils


@pytest.fixture
def mock_export():
    def do_export(model, path, hyperparams, const_cls, forced=False):
        with open(path, 'w') as stream:
            stream.write('I was exported! Forced={}'.format(forced))

    old_export = models_utils.export_keras_model
    models_utils.export_keras_model = do_export
    yield True
    models_utils.export_keras_model = old_export


@pytest.mark.parametrize('classifier,args,kwargs,exp_msg', [
    ('foo', [1, 2, 3], {}, 'Training for this model is not currently '
                           'supported.'),
])
def test_train_unsupported(classifier, args, kwargs, exp_msg):
    with pytest.raises(Exception) as err:
        TRAIN_HANDLERS[classifier](*args, **kwargs)

    assert exp_msg in str(err.value)


@pytest.mark.parametrize('args,kwargs', [
    ([(np.array([[1, 2, 3]]), np.array([[2, 3, 4]]), np.array([0]))], {}),
    ([(np.array([[1, 2, 3]]), np.array([[2, 3, 4]]), np.array([0]))],
     {'save_location': 'outdir.join("saved_model.txt")'}),
    ([(np.array([[1, 2, 3]]), np.array([[2, 3, 4]]), np.array([0]))],
     {'export_location': 'outdir.join("exported_model.txt")'}),
])
def test_train_graph_conv_net(args, kwargs, outdir, mock_export):
    logs_file = outdir.join('logs.txt')
    exp_logs = ['fit']

    model = MockKerasClassifier(str(logs_file), const.GraphConvNet)
    hyperparams = models_utils.HyperParameters()
    args.append(hyperparams)

    for kw in kwargs:
        kwargs[kw] = str(eval(kwargs[kw]))
    TRAIN_HANDLERS['graph_conv_net'](model, *args, **kwargs)

    if 'save_location' in kwargs:
        with open(kwargs['save_location']) as stream:
            assert 'I was saved!' in stream.read()

    if 'export_location' in kwargs:
        with open(kwargs['export_location']) as stream:
            assert 'I was exported! Forced=False' in stream.read()

    assert exp_logs == logs_file.read().split('\n')[:-1]


@pytest.mark.parametrize('data_in', [['foo'], ['foo', 'bar', 'baz', 'bam']])
def test_train_graph_conv_net_invalid_args(data_in, outdir):
    logs_file = outdir.join('logs.txt')
    model = MockKerasClassifier(str(logs_file), const.GraphConvNet)
    hyperparams = models_utils.HyperParameters()
    exp_msg = ('The data parameter must have exactly 3 arguments. Got {}. '
               'Check the docstring for further '
               'information.'.format(len(data_in)))
    with pytest.raises(Exception) as err:
        TRAIN_HANDLERS['graph_conv_net'](model, data_in, hyperparams)

    assert exp_msg in str(err.value)


def test_train_with_hyperparameters(outdir):
    logs_file = outdir.join('logs.txt')
    exp_logs = ['fit']

    hyperparams = models_utils.HyperParameters()
    hyperparams.add('epochs', 100)
    hyperparams.add('batch_size', 10)

    model = MockKerasClassifier(
        str(logs_file), const.GraphConvNet, hyperparameters=hyperparams,
    )
    TRAIN_HANDLERS['graph_conv_net'](
        model, (np.array([[1, 2, 3]]), np.array([[2, 3, 4]]), np.array([0])),
        hyperparams,
    )

    assert exp_logs == logs_file.read().split('\n')[:-1]
