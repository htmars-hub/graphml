# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest
import tensorflow.keras as keras

from abp.graphml.models import utils
from abp.graphml.models import constants as const
from abp.graphml.models import exceptions as err


def mock_saved_model_function(model, location):
    if location == 'existent_location':
        raise AssertionError('Export directory already exists')

    if location == 'error_location':
        raise AssertionError('Error being passed down!')

    assert isinstance(model, keras.Model)


@pytest.fixture
def empty_keras_model():
    return keras.Model()


def test_export_keras_model():
    pass


def test_hyperparameters_add():
    hyperparaeters = utils.HyperParameters()

    for hp in const.HYPERPARAMS_TYPES:
        assert hyperparaeters.add(name=hp, value=const.HYPERPARAMS_TYPES[hp]())


def test_hyperparameters_add_error():
    hyperparameters = utils.HyperParameters()

    # Wrong hyperparameter name
    with pytest.raises(err.HyperparameterTypeError) as error:
        hyperparameters.add('invalid', None)
    assert 'is not a valid hyperparameter' in str(error.value)

    # Wrong hyperparameter type
    with pytest.raises(err.HyperparameterTypeError) as error:
        hyperparameters.add('epochs', '700')
    assert 'is not a valid hyperparameter' in str(error.value)


@pytest.mark.parametrize('to_add,get_name,get_default,exp_out', [
    (('epochs', 20), 'epochs', None, 20),
    (('batch_size', 128), 'batch_size', 128, 128),
])
def test_hyperparamters_get(to_add, get_name, get_default, exp_out):
    hyperparameters = utils.HyperParameters()
    hyperparameters.add(*to_add)

    assert hyperparameters.get(get_name, get_default) == exp_out
