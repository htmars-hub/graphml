# This file is part of the ABP Graph Classifier.
# Copyright (C) 2019 Eyeo GmbH
#
# ABP Graph Classifier is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ABP Graph Classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ABP Graph Classifier. If not,
# see <http://www.gnu.org/licenses/>.

import pytest
import numpy as np

from abp.graphml.models.actions import PREDICT_HANDLERS
from tests.utils import MockKerasClassifier
import abp.graphml.models.constants as const


@pytest.mark.parametrize('classifier,args,kwargs,exp_msg', [
    ('foo', [1, 2, 3], {}, 'Predicting for this model is not currently '
                           'supported.'),
])
def test_train_unsupported(classifier, args, kwargs, exp_msg):
    with pytest.raises(Exception) as err:
        PREDICT_HANDLERS[classifier](*args, **kwargs)

    assert exp_msg in str(err.value)


@pytest.mark.parametrize('as_probs,verbose,exp_log', [
    (True, 0, ['predict_proba - verbose=0']),
    (False, 0, ['predict - verbose=0']),
    (False, 1, ['predict - verbose=1']),
])
def test_graph_conv_net(as_probs, verbose, exp_log, outdir):
    logs_file = outdir.join('logs.txt')
    classifier = MockKerasClassifier(str(logs_file), const.GraphConvNet)
    data = [np.array([1, 2, 3]), np.array([1, 0, 1])]

    PREDICT_HANDLERS['graph_conv_net'](classifier, data, as_probs, verbose)

    assert exp_log == logs_file.read().split('\n')[:-1]


@pytest.mark.parametrize('data_in', [['foo'], ['foo', 'bar', 'baz']])
def test_graph_conv_net_invalid_args(data_in):
    exp_msg = ('The length of the data tuple has to be exactly 2. Found {}. '
               'Check the docstring for further '
               'information'.format(len(data_in)))

    with pytest.raises(ValueError) as err:
        PREDICT_HANDLERS['graph_conv_net']('mock', data_in)

    assert exp_msg in str(err.value)
